
# Compression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**level** | **Integer** | Compression level. Valid range from 0 (minimal compression) to 10 (maximal compression) |  [optional]
**type** | [**TypeEnum**](#TypeEnum) | Compression type. ADVANCED is only allowed and should be used with the ADVANCED engine |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
NONE | &quot;NONE&quot;
ADVANCED | &quot;ADVANCED&quot;



