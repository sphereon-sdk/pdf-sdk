# Sphereon.SDK.Pdf.Model.ConversionSettings
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Lifecycle** | [**Lifecycle**](Lifecycle.md) |  | [optional] 
**QualityFactor** | **int?** | Set the quality factor for the resulting PDF. Range from 0 (lowest) to 10 (highest) | [optional] 
**OutputFileName** | **string** |  | [optional] 
**Engine** | **string** |  | [optional] 
**Compression** | [**Compression**](Compression.md) |  | [optional] 
**ContainerConversion** | **string** |  | [optional] 
**Version** | **string** |  | [optional] 
**OutputFileFormat** | **string** |  | [optional] 
**OcrMode** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

