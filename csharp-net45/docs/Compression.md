# Sphereon.SDK.Pdf.Model.Compression
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Level** | **int?** | Compression level. Valid range from 0 (minimal compression) to 10 (maximal compression) | [optional] 
**Type** | **string** | Compression type. ADVANCED is only allowed and should be used with the ADVANCED engine | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

